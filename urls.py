﻿# -*- coding:utf-8 -*-
from os.path import join, dirname
from datetime import datetime

from django.conf import settings
from django.conf.urls.defaults import *
from django.contrib import sitemaps

from django.contrib.sitemaps import GenericSitemap

from registration.forms import *
from mysitemaps import *



# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

"""
news_info_dict = {
    'queryset': NewsSitemap.items,
    'date_field': datetime.now,
}
"""

sitemaps = {        
    'news': NewsSitemap,
    'pages': StaticpagesSitemap,
    'load': DownloadsSitemap,
    'help': HelpSitemap,
}

urlpatterns = patterns('',
    # i18n:
    (r'^i18n/', include('django.conf.urls.i18n')),
    #(r'^tinymce/', include('tinymce.urls')),

    #sitemaps
    (r'^sitemap.xml$', 'django.contrib.sitemaps.views.sitemap', {'sitemaps' : sitemaps}),
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/(.*)', admin.site.root, name='admin'),
    #(r'^admin/', include('admin.site.urls')),

    url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^static/(.*)$', 'django.views.static.serve',{'document_root': settings.STATIC_ROOT}),
    url(r'^admin-media/(.*)$', 'django.views.static.serve', {'document_root': join(dirname(admin.__file__), 'media')}),
    url(r'^/register/?', 'registration.views.register', {'form': RegistrationFormUniqueEmail}, name='registration_register'),    
    (r'^accounts/', include('registration.urls')),	
    #(r'^forum/(.*)$', include('argusp.forum.urls')),	
    #(r'^wiki(.*)$', include('simplewiki.urls')),
    #forum        
    (r'^forum/', include('forum.urls')),
    #faqch        
    (r'^faqch/', include('faqch.urls')),
    #wiki    
    #(r'^wiki', include('arguswiki.urls')),
    (r'^documentation', include('arguswiki.urls')),
    (r'^help', include('help.urls')),
    #faq
    #url(r'^faq/?', 'faq.views.list', name="faq_list"),
    (r'^faq', include('faq.urls')),
    #News    
    (r'^news', include('news.urls')),
    #download    
    (r'^download', include('downloads.urls')),
    #download    
    (r'^reviews', include('reviews.urls')),
    #pyamf
    url(r'^getawey', include('pyamftest.urls')),
    #Static Pages
    url(r'^contacts/?', 'staticpages.views.viewpage', {'_typepage': 'CN', 'extracontext': {'link':'contacts'}, 'contacts': True},  name='contacts'),
    url(r'^company/?', 'staticpages.views.viewpage', {'_typepage': 'AC', 'extracontext': {'link':'company'}},  name='company'),
    url(r'^/?', 'staticpages.views.viewpage', {'_typepage': 'AG', 'extracontext': {'link':'about'}},  name='about'),
)

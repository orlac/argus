# -*- coding:utf8 -*-
from django.template import Library, Node
register = Library()

from arguswiki.models import *

"""
def load_category(parser, arg):
	
	html = '<ul class="load_categoty">'
	q = Category.objects.all()
	for n in q:
		html += '<li>%s</li>' % n.name
	html += '</ul>'
	return html
	
	q = Category.objects.all()
	return q
"""
	

def load_category_child(parser, token):
    """
    {% forum_latest_posts [number] as [context_var] %}
    """
    bits = token.contents.split()
    if len(bits) not in (1, 2, 4):
        raise TemplateSyntaxError('%s tag requires none, one or three arguments' % bits[0])
    if bits[2] != 'as':
        raise TemplateSyntaxError("Second argument to %s tag must be 'as'" % bits[0])
    if not bits[1]:
        bits[1] = 5 # Default number of items
    if not bits[3]:
        bits[3] = 'latest_posts'
    return CategoryLatestPostsNode(bits[1], bits[3])

class CategoryLatestPostsNode(Node):
    def __init__(self, number, context_var):
        self.number = int(number)
        self.context_var = context_var
    
    def render(self, context):
        #context[self.context_var] = Category.objects.filter(parent=False)
        context[self.context_var] = [(node, node.descendants_count) for node in Category.get_descendants_group_count()]
        return ''



register.tag(load_category_child)
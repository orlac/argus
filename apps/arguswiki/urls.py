﻿# -*- coding:utf-8 -*-
from django.conf.urls.defaults import *
from arguswiki import views
#from doplace.views import *

urlpatterns = patterns(
    '',
    #url(r'^search/?$', views.search, name="search_place"),
    #url(r'^place/(?P<idplace>\d{1,5})?', views.list, name="list_place"),
    #(r'^users/(?P<nameuser>\w+$)', 'doplace.views.userprofile', ),
    #(r'^users/?', 'doplace.views.users', ),
    #(r'^newplace/?', 'doplace.views.newplace', ),
    url(r'^/argus/doc/(?P<iddoc>\d{1,10})', views.showdoc, {'template_name': 'desc/wiki_doc.html'}, name="descwiki_show"),
    url(r'^/argus/docs/(?P<iddoc>\d{1,10})', views.showdocs, {'template_name': 'desc/wiki_docslist.html'}, name="descwiki_showdocs"),
    url(r'^/argus/(?P<idparent>\d{1,10})?', views.list, {'template_name': 'desc/wiki_list.html'}, name="descwiki_list"),
    
    url(r'^/doc/(?P<iddoc>\d{1,10})', views.showdoc, {'extracontext': {'link':'doc'}}, name="wiki_show"),
    url(r'^/docs/(?P<iddoc>\d{1,10})', views.showdocs, {'extracontext': {'link':'doc'}}, name="wiki_showdocs"),
    url(r'^/(?P<idparent>\d{1,10})?', views.list, {'extracontext': {'link':'doc'}}, name="wiki_list"),               
)

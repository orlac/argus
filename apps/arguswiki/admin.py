# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.comments.models import *

from treebeard.admin import TreeAdmin

from arguswiki.models import *
#from django.contrib.contenttypes.generic import GenericTabularInline


#select_score = Post.objects.select_score

"""
class CommentInline(GenericTabularInline):
    model = Comment
    ct_fk_field = 'object_pk'
    extra = 1
    fields = ('user', 'submit_date', 'comment', 'is_public', 'is_removed')
"""

class SorlInline(admin.TabularInline):    
    list_display = ('photo',)
    model = SorlPhotos
    extra = 3
    


class DocsAdmin(admin.ModelAdmin):
    inlines = [
        SorlInline,
       ]
    list_display = ('__unicode__', 'author', 'by_date', 'parent')
    raw_id_fields = ['author']
    #list_filter = ('parent', 'by_date')
    #fields = ('user', 'name', 'by_date', 'text',)

    save_on_top = True
    pass


class DocsInline(admin.TabularInline):    
    inlines = [
        SorlInline,
       ]
    list_display = ('name',)
    model = Docs
    extra = 1
    

class CategoryAdmin(TreeAdmin):
    inlines = [
        DocsInline,        
       ]    
    list_display = ('name', 'content')
    #list_filter = ('parent')
    #fields = ('user', 'name', 'by_date', 'text',)

    save_on_top = True
    pass


 
#admin.site.unregister(Comment)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Docs, DocsAdmin)
#admin.site.register(SorlPhotos, SorlAdmin)
#admin.site.register(Place)
 

 
$(function(){

// Dialog
$('#dialog').dialog({
          autoOpen: false,
          width: 600,
          buttons: {
              "Ok": function() {
              $(this).dialog("close");
              },
              "Cancel": function() {
              $(this).dialog("close");
              },
              "Refresh": function() {
              getPlace(0);
              }  
            },
          show: "slide",
          open: function(event, ui) {
			getPlace(0);
            },
     });


// Dialog Link
$('#dialog_link').click(function(){
      $('#dialog').dialog('open');
      return false;
     });



$('#select_place').change(function() {
  var id = $('#select_place option:selected').val();
  $('#id_SelectPlace').attr('value', id);
  getPlace(id);
});

});


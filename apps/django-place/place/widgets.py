# -*- coding:utf8 -*-
from django.forms.widgets import *
from django.utils.safestring import mark_safe
from django.utils.encoding import force_unicode
from django.forms.util import flatatt
from django.conf import settings


class SelectPlaceWidget(HiddenInput):
    """
    Widget Выпадающий список jquery ui dialog
    """

    class Media:
        js = (settings.MEDIA_URL+'js/place/jquery-1.3.2.min.js',
              settings.MEDIA_URL+'js/place/jquery-ui-1.7.2.custom.min.js',
              settings.MEDIA_URL+'js/place/opendialog.js',
              #settings.MEDIA_URL+'js/place/getip.js',
              settings.MEDIA_URL+'js/place/places.js',
                )
        css = {'' : (settings.MEDIA_URL+'css/place/ui-lightness/jquery-ui-1.7.2.custom.css',)}

    """
    def __init__(self, attrs={}):
        super(SelectPlaceWidget, self).__init__(attrs={'class': '', 'size': '', 'id': 'dialog'})
    """
    """
    def __init__(self, attrs=None):
        super(SelectPlaceWidget, self).__init__(attrs)
    """

    def render(self, name, value=0, attrs=None):
        if value is None: value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name, value=value)
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = value
                    
        div_dialog = u'<input%s /><div id="dialog" title="Dialog Title"><select id = "select_place"></select></div>\n<a id="dialog_link" class="ui-state-default ui-corner-all" href="#">Select Place</a>' % flatatt(final_attrs)
        
        
        return mark_safe(div_dialog)

        #div_dialog = u'<input type="hidden" name="'+name+'" id="'+name+'" value=""><div id="dialog" title="Dialog Title"><select id = "select_place"></select></div>'
        #return mark_safe(div_dialog+u'<a id="dialog_link" class="ui-state-default ui-corner-all" href="#">Select Place</a>')
 

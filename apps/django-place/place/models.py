# -*- coding:utf8 -*-
from django.db import models
#from treebeard.mp_tree import MP_Node


class CountryInfo(models.Model):
    iso_alpha2 = models.CharField(max_length = 4)
    iso_alpha3 = models.CharField(max_length = 4)
    iso_numeric = models.CharField(max_length = 4)
    fips_code = models.CharField(max_length = 4)
    name = models.CharField(max_length = 50)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = 'Countries'

        # when adding a custom Meta class to a MP model, the ordering must be
        # set again
        ordering = ['name']


#Наследуемся от этой модели
class Place(models.Model):
    country = models.ForeignKey('CountryInfo', related_name='CountryInfo')
    adm1 = models.IntegerField(null = True, blank = True)
    adm2 = models.IntegerField(null = True, blank = True)
    adm3 = models.IntegerField(null = True, blank = True)
    adm4 = models.IntegerField(null = True, blank = True)
    adm5 = models.IntegerField(null = True, blank = True)
    ppl = models.IntegerField(null = True, blank = True)

    def __unicode__(self):
       return  self.id

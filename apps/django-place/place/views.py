# -*- coding:utf8 -*-
from django.shortcuts import redirect
from django.http import HttpResponse
from django.utils.encoding import smart_unicode
from django.utils import simplejson


def getall(request, geonameid = 0, redir_url = "http://ws.geonames.org/childrenJSON?geonameId="):
	if geonameid != 0:
		redir_url += str(int(geonameid))
		return redirect(redir_url)



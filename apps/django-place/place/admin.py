# -*- coding:utf-8 -*-
from django.contrib import admin
from django.conf import settings
from django import forms
from place.models import *
from place.forms import *


class SelectPlaceModelForm(forms.ModelForm):
    SelectPlace = SelectPlaceField()

    class Meta:
        model = Place
    pass



class PlaceAdmin(admin.ModelAdmin):
      form = SelectPlaceModelForm
      pass


admin.site.register(Place, PlaceAdmin)
#admin.site.register(CountryInfo)       89518577777 

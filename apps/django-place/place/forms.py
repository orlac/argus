# -*- coding:utf8 -*-
from django import forms
from place import widgets
from django.utils.importlib import import_module
from django.utils.translation import ugettext_lazy as _



class SelectPlaceField(forms.Field):
    widget = widgets.SelectPlaceWidget
    default_error_messages = {
        'invalid': _(u'Enter a valid place.'),
    }

    def __init__(self, *args, **kwargs):
        forms.Field.__init__(self, *args, **kwargs)

    def clean(self, value):
        """
        Validate of geo-place
        """
        
        return value




class SelectPlaceForm(forms.Form):
    """
    Form to select place
    """
    SelectPlace = SelectPlaceField()

    class Media:
        js = ('http://ajax.googleapis.com/ajax/libs/jquery/1.4.0/jquery.min.js',
                '/site_media/js/municipality.js')

    def clean(self):
        return self.cleaned_data

"""
class SelectPlaceModelForm(forms.ModelForm, SelectPlaceForm):
    def __init__(self, namemodel):
        self.model = namemodel

    class Meta:
        model = self.model
"""

# -*- coding:utf8 -*-
from django.conf.urls.defaults import *
from place.views import *


urlpatterns = patterns('',
    # Example:
    (r'^getall/(?P<geonameid>\d{1,10})?', 'place.views.getall',),
)

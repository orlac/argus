# -*- coding:utf-8 -*-
STATIC_PAGES=(
    ('AG', 'О продукте'),
    ('AC', 'О компании'),
    ('CN', 'Контакты'),
) 

#Новостей на страницу
NEWSPAGE = 2
#Вопросов на страницу
FAQSPAGE = 3
#Отзывов на страницу
REVIEWPAGE = 2

#Регистрация_________________________________________________________
#Время жизни неактивных акаунтов
ACCOUNT_ACTIVATION_DAYS = 2
#Отпрвлять код на почту
AUTH_USER_EMAIL_UNIQUE = True

#Настройки сервера
EMAIL_HOST = 'localhost'
EMAIL_PORT = 1025
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'info@google.ru'

LOGIN_REDIRECT_URL = '/forum/' #редирект при авторизации
AUTH_PROFILE_MODULE = 'Registration.models.RegistrationProfile'
#____________________________________________________________________

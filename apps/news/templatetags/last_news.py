# -*- coding:utf8 -*-
from django.template import Library, Node
register = Library()

from news.models import *
	

def load_last_news(parser, token):
    """
    {% load_last_news [count] as [context_var] %}
    """
    bits = token.contents.split()
    if len(bits) not in (1, 2, 4):
        raise TemplateSyntaxError('%s tag requires none, one or three arguments' % bits[0])
    if bits[2] != 'as':
        raise TemplateSyntaxError("Second argument to %s tag must be 'as'" % bits[0])
    if not bits[1]:
        bits[1] = 5 # Default number of items
    if not bits[3]:
        bits[3] = 'latest_posts'
    return NewsLatestPostsNode(bits[1], bits[3])

class NewsLatestPostsNode(Node):
    def __init__(self, number, context_var):
        self.number = int(number)
        self.context_var = context_var
    
    def render(self, context):
        context[self.context_var] = News.objects.all().order_by("-by_date")[:self.number]
        return ''



register.tag(load_last_news)
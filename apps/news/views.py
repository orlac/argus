# -*- coding:utf8 -*-
import datetime

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import Http404

from news.models import *


"""
def search(request, template_name='doplace/search.html', extracontext={}):
    query=False
    if request.method =='POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            query=query_list(request, data=form.cleaned_data)
    else:
        form = SearchForm()
    return render_to_response(template_name, {'query':query, 'form_search':form}, context_instance=RequestContext(request, extracontext))
"""

def list(request, ordering="-by_date", template_name="news_list.html", extracontext={}, amf = False):

    try:
        query=News.objects.all().order_by(ordering)
    except:
        query=False
    if amf:
        return {'query':query, 'request':request, 'extracontext':extracontext}
    return render_to_response(template_name, {'query':query}, context_instance=RequestContext(request, extracontext))

def amf_list(ordering="-by_date", template_name="news_list.html", extracontext={}, amf = False):
    try:
        query=News.objects.all().order_by(ordering)
    except:
        query=False
    if amf:
        return {'query':query, 'extracontext':extracontext}    



def show(request, idnews=0, template_name="news_show.html", extracontext={}):
    idnews = int(idnews)
    if idnews != 0:
        try:
            query=News.objects.get(id=idnews)
        except:
            query=False
        picts = SorlPhotos.objects.filter(news = idnews)
    else:
        query=False
    return render_to_response(template_name, {'query':query, 'picts': picts}, context_instance=RequestContext(request, extracontext))




"""
@login_required
def newplace(request, template_name='doplace/newplace.html', extracontext={}, redir_url="/"):
    if request.method =='POST':
        form = NewPlaceForm(request.POST)
        if form.is_valid():
            try:
                p = Place.objects.get(name=form.cleaned_data['place'])
            except:
                p = Place(name=form.cleaned_data['place'])
                p.save()
            new_place = Doing(place=p,
                                  do=form.cleaned_data['do'],
                                  user=request.user,
                                  by_date = datetime.datetime.now(),
                                  )
            new_place.save()
            return redirect("list_place", int(p.id))
    else:
        form = NewPlaceForm()
    return render_to_response(template_name, {'form_new_place':form}, context_instance=RequestContext(request, extracontext))


def query_list(request, data={}):
    query=False
    if 'name' in data and 'place' in data :
        query = Doing.objects.filter(user__username=data['name'], place__name=data['place'])
    elif  'name' in data and 'place' not in data:
        query = Doing.objects.filter(user__username=data['name'])
    elif 'name' not in data and 'place' in data:
        query = Doing.objects.filter(place__name=data['place'])
    return query
"""





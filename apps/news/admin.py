# -*- coding:utf-8 -*-
from django.contrib import admin
from django import forms
from django.contrib.comments.models import *


from news.models import *
from news.forms import *
#from photologue.models import *
#from django.contrib.contenttypes.generic import GenericTabularInline


class ImageModelForm(forms.ModelForm):
    photo = MyImageField()

    class Meta:
        model = Photos
    pass


"""
class SorlAdmin(admin.ModelAdmin):
    list_display = ('photo',)


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('photo',)
    pass
"""


#select_score = Post.objects.select_score

"""
class CommentInline(GenericTabularInline):
    model = Comment
    ct_fk_field = 'object_pk'
    extra = 1
    fields = ('user', 'submit_date', 'comment', 'is_public', 'is_removed')
"""


"""
class PhotosInline(admin.TabularInline):
    list_display = ('photo',)
    model = Photos
    form = ImageModelForm
    extra = 1
"""



class SorlInline(admin.TabularInline):
    list_display = ('photo',)
    model = SorlPhotos
    extra = 5




class NewsAdmin(admin.ModelAdmin):

    inlines = [
        #PhotosInline,
        SorlInline,
       ]

    raw_id_fields = ['user']
    list_filter = ('user', 'by_date',)
    fields = ('user', 'name', 'by_date', 'text',)

    """formfield_overrides = {
        models.TextField: {'widget': RichTextEditorWidget},
    }"""
	#fields = ('user', 'submit_date', 'comment', 'is_public', 'is_removed')
    #radio_fields = {"parent": admin.HORIZONTAL}
    #Кнопка сохранить вверху
    save_on_top = True
    pass

#admin.site.unregister(Comment)
admin.site.register(News, NewsAdmin)
#admin.site.register(Photos, PhotoAdmin)
#admin.site.register(SorlPhotos, SorlAdmin)
#admin.site.register(Place)
 

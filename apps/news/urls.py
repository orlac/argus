# -*- coding:utf-8 -*-
from django.conf.urls.defaults import *
from news import views
#from doplace.views import *

urlpatterns = patterns(
    '',
    #url(r'^search/?$', views.search, name="search_place"),
    #url(r'^place/(?P<idplace>\d{1,5})?', views.list, name="list_place"),
    #(r'^users/(?P<nameuser>\w+$)', 'doplace.views.userprofile', ),
    #(r'^users/?', 'doplace.views.users', ),
    #(r'^newplace/?', 'doplace.views.newplace', ),
    url(r'^(?P<idnews>\d{1,5})', views.show, {'extracontext': {'link':'news'}}, name="news_show"),
    url(r'^/?', views.list, {'extracontext': {'link':'news'}}, name="news_list"),
)

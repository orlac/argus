# -*- coding:utf8 -*-
from django import forms
from django.forms.widgets import *
from django.db.models.fields.files import ImageField
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin.widgets import AdminFileWidget
from django.utils.safestring import mark_safe


class MyImageFieldWidget(AdminFileWidget):

	"""
    def __init__(self, attrs={}):
		super(MyImageFieldWidget, self).__init__(attrs)

	
	def render(self, name, value, attrs=None):
		output = []
		if value and hasattr(value, "url"):
			output.append('%s <a target="_blank" href="%s">%s</a> <br />%s ' % \
				(_('Currently:'), value.url, value, _('Change:')))
        output.append( super(MyImageFieldWidget, self).render(name, value, attrs) )
        
        return mark_safe('dsff')
    """


class MyImageField(forms.ImageField):
    widget = MyImageFieldWidget    
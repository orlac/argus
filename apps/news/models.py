# -*- coding:utf-8 -*-
from datetime import datetime
import random

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

#from django.contrib.comments.moderation import CommentModerator, moderator
from imagekit.fields import *
from imagekit.models import ImageModel
#from photologue.models import *
from sorl.thumbnail.fields import *


class News(models.Model):
    user = models.ForeignKey(User, )    
    name = models.CharField(max_length=100, )
    text = models.TextField(max_length=500, )
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)
    #galery = models.OneToOneField(Gallery, null=True, blank=True)


    def __unicode__(self):
        return u"%s  | %s : %s |" %(self.name ,self.user, self.by_date)
        
    def get_absolute_url(self):
        return reverse('news_show', args=[self.id])
    
    def all_phots(self):
    	return SorlPhotos.objects.filter(news = self)
    
    """
    def save(self):

        # Тэйс отладчик
        # Сохраняем запись без галереи
        #self.save()

        try:
            g = Gallery.objects.get(title = "%s|%s" % ('News', self.id))
            self.save()
        except:
            g = Gallery(title = "%s|%s" % ('News', self.id),
                   title_slug = "%s|%s" % ('News', self.id),
            )
            g.save()
            self.galery = g
            self.save()
        #_title = str(random.randrange(99999))
        #_title_slug = str(random.randrange(99999))
        #print g
        #self.galery = g
        #print g.id
        #self.save()
    """

    class Meta: 
        ordering = ['-by_date']
        db_table = settings.DBPREFIX+'news'
        verbose_name = _("News")
        verbose_name_plural = _("News")
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """

class SorlPhotos(models.Model):
    news = models.ForeignKey('News',)
    photo = ThumbnailField(upload_to='Sorlfiles/news',
                           size=(200, 200),
                           extra_thumbnails={
                           'icon': {'size': (50, 50)},
                           }
                       )
    name = models.CharField(max_length=50, null=True, blank=True)


    class Meta: 
        db_table = settings.DBPREFIX+'news_SorlPhoto'
        verbose_name = _("SorlPhoto")
        verbose_name_plural = _("SorlPhoto")






class Photos(ImageModel):
    news = models.ForeignKey('News',)
    photo = GaleryField(upload_to='imagekit/news',
                           thumb_size={'x':500, 'y':500},
                           )
    num_views = models.PositiveIntegerField(editable=False, default=0)

    #imagekit______
    class IKOptions:
        # This inner class is where we define the ImageKit options for the model
        spec_module = 'news.specs'
        cache_dir = 'cache_dir/news'
        image_field = 'photo'
        save_count_as = 'num_views'



"""
sclass PostModerator(CommentModerator):
    email_notification = True
    auto_close_field   = 'by_date'
    Close the comments after 7 days.
    close_after        = 7

moderator.register(Post, PostModerator)
"""

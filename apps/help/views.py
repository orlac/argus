# -*- coding:utf8 -*-
import datetime

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import Http404
from django.shortcuts import render_to_response, get_object_or_404
from django.db import connection
#from django.shortcuts import redirect
from django.http import HttpResponseRedirect as redirect

from help.models import *
#from django.contrib.auth.decorators import login_required


def list(request, idparent=None, template_name="wiki_list.html", extracontext={}):
    
    """ convo view~
    """    
    data = {'sql_queries': connection.queries,}
    if idparent:
        root = get_object_or_404(Category, id=idparent)
        if root.get_depth() != 1:
            # meh not really a root node, redirecting...
            return HttpResponseRedirect('%s#comment_%d' % (root.get_root().get_absolute_url(), root.id))
    else:
        root = None    
    
    if idparent:        
    	data['root'] = False
        descendants = root.get_descendants()
        nodes = [(root, len(descendants))] + [ (node, node.get_children_count())  for node in descendants ]        
        data['nodes'] = nodes        
    else:        
        data['root'] = True
        data['nodes'] = [ (node, node.descendants_count) for node in Category.get_descendants_group_count()]
        data['total_docs'] = len(data['nodes']) + sum([count for _, count in data['nodes']])        
		
    return render_to_response(template_name, {'query':data, 'idparent': idparent}, context_instance=RequestContext(request, extracontext))


def showdocs(request, iddoc=0, template_name="wiki_docslist.html", extracontext={}):
    iddoc = int(iddoc)
    if iddoc != 0:
        try:
			parent=Category.objects.get(id=iddoc)
			query=Docs.objects.filter(parent=iddoc)            
        except:
            query=False
            parent=False
    else:
        query=False
    return render_to_response(template_name, {'query':query, 'parent': parent}, context_instance=RequestContext(request, extracontext))


def showdoc(request, iddoc=0, template_name="wiki_doc.html", extracontext={}):
    iddoc = int(iddoc)
    if iddoc != 0:
        try:
            query=Docs.objects.get(id=iddoc)
        except:
            query=False
        picts = SorlPhotos.objects.filter(doc = iddoc)
    else:
        query=False
    return render_to_response(template_name, {'query':query, 'picts': picts}, context_instance=RequestContext(request, extracontext))




# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.comments.models import *

from treebeard.admin import TreeAdmin

from help.models import *
#from django.contrib.contenttypes.generic import GenericTabularInline


#select_score = Post.objects.select_score




class SorlCatsInline(admin.TabularInline):    
    list_display = ('photo',)
    model = SorlCatsPhotos
    extra = 3    



 

class CategoryAdmin(TreeAdmin):
    inlines = [
        SorlCatsInline,
       ]    
    list_display = ('name', 'content')
    #list_filter = ('parent')
    #fields = ('user', 'name', 'by_date', 'text',)

    save_on_top = True
    pass


 
#admin.site.unregister(Comment)
admin.site.register(Category, CategoryAdmin)
#admin.site.register(Docs, DocsAdmin)
#admin.site.register(SorlPhotos, SorlAdmin)
#admin.site.register(Place)
 

 
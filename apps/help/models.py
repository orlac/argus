# -*- coding:utf8 -*-
import os, sys
from datetime import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import User

#from treebeard.mp_tree import MP_Node
from treebeard.al_tree import AL_Node
#from treebeard.ns_tree import NS_Node
from sorl.thumbnail.fields import *



# Create your models here.

class Category(AL_Node):
    name = models.CharField(max_length=100, )
    content = models.TextField(_('description'), max_length=500, null=True, blank=True)
    parent = models.ForeignKey('self', related_name='children_set', null=True, blank=True, db_index=True)            

    node_order_by = ['name']

    @models.permalink
    def get_absolute_url(self):
        return ('node-view', ('al', str(self.id),))

    def __unicode__(self):
        return  u" %s | " % (self.name)
        
    def get_phots(self):
        return SorlCatsPhotos.objects.filter(cat = self)

    class Meta: 
        ordering = ['name']
        db_table = settings.DBPREFIX+'categotyhelp'
        verbose_name = _("Category")
        verbose_name_plural = _("Category")



class SorlCatsPhotos(models.Model):
    cat = models.ForeignKey('Category',)    
    photo = ThumbnailField(upload_to='Sorlfiles/Catdocs',
                           size=(950, 950),
                           extra_thumbnails={
                           'icon': {'size': (50, 50)},
                           }
                       )
    name = models.CharField(max_length=50, null=True, blank=True)


    class Meta: 
        db_table = settings.DBPREFIX+'cathelp_SorlPhoto'
        verbose_name = _("CatsDocsPhoto")
        verbose_name_plural = _("CatsDocsPhoto")        
        

# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin

from invite.models import *

#action отправка почты выбранным пользователям
def send_mail(modeladmin,  request, queryset):

    for inv in queryset:
       Invite.send_mail(inv)
       inv.is_send = True
       inv.by_date = ''
       inv.save()
    return inv

send_mail.short_description = "Send email to these people"


class InviteAdmin(admin.ModelAdmin):
      raw_id_fields = ['user']
      list_display = ('__unicode__', 'to_email', 'is_invite', 'is_send', 'by_date',)
      list_filter = ('is_invite', 'is_send',)
      fields = ('user', 'email',)
      actions = [send_mail]
      pass

class InviteProfileAdmin(admin.ModelAdmin):
      raw_id_fields = ['user']
      #list_display = ('__unicode__', 'to_email', 'is_invite', 'is_send', 'by_date',)
      #list_filter = ('is_invite', 'user',)
      #fields = ('user', 'email',)
      #actions = [send_mail]
      pass

class InviteMessagesAdmin(admin.ModelAdmin):
      raw_id_fields = ['user', 'by_user']
      #list_display = ('__unicode__', 'to_email', 'is_invite', 'is_send', 'by_date',)
      #list_filter = ('is_invite', 'user',)
      #fields = ('user', 'email',)
      #actions = [send_mail]
      pass

admin.site.register(InviteProfile, InviteProfileAdmin)
admin.site.register(InviteMessages, InviteMessagesAdmin)
admin.site.register(Invite, InviteAdmin)


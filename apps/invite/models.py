# -*- coding:utf8 -*-
import datetime
import random
import re

from django.conf import settings
from django.contrib.auth.models import User, Group
from django.db import models
#from django.db import transaction
from django.template.loader import render_to_string
from django.utils.hashcompat import sha_constructor
from django.utils.translation import ugettext_lazy as _

from django.core.mail import send_mail

#Надо указать в настройках
#ProfileModel = 'account.AcountProfile'
#AccountModel = get_model(ProfileModel.split('.'))


class InviteManager(models.Manager):
    def create_invite(self, user, email, by_date):

        if not by_date:
            by_date = datetime.datetime.now()
        kwargs = {'user': user, 'email': email, 'by_date': by_date}

        kwargs['code'] = str(random.randrange(1111111,9999999 , 7))

        if settings.SEND_MAIL_INVITE and settings.SEND_MAIL_INVITE == True:
            self.send_mail(self)

        return self.create(**kwargs)



class Invite(models.Model):
    user = models.ForeignKey(User, verbose_name=_('Inviting of'))
    email = models.EmailField(max_length=75)
    key = models.CharField(max_length=40, null = True, blank = True)
    by_date = models.DateTimeField(auto_now=True, auto_now_add=True,)
    is_send = models.BooleanField(default=False,)
    is_invite = models.BooleanField(default=False,)


    objects = InviteManager()


    def __unicode__(self):
        return u"invite of %s" % self.user


    def to_email(self):
        return u"to %s" % self.email



    def send_mail(self):
        mail_context = { 'key': self.key,
                      'user': self.user,
                      'inviting_count_days': settings.INVITING_COUNT_DAYS,
                      }
        subject = render_to_string('invite/invite_subject.txt', mail_context)
        subject = ''.join(subject.splitlines())


        message = render_to_string('invite/invite_email.txt', mail_context)

        #test:
        #python -m smtpd -n -c DebuggingServer localhost:1025
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [self.email], fail_silently=False)

    def create(self):
        self.objects.create_invite(self.user, self.email, self.by_date)



#set this  AUTH_PROFILE_MODULE in settings

class InviteProfile(models.Model):
      user = models.ForeignKey(User, unique=True, verbose_name=_('user'))
      activation_key = models.CharField(_('activation key'), max_length=40, blank = True)


      def __unicode__(self):
          return u"Profile information for %s" % self.user


class InviteMessages(models.Model):
      user = models.ForeignKey(User,)
      message = models.TextField(_('Text message'), max_length=200,)
      by_user = models.ForeignKey(User, related_name='message_from_user')


      def __unicode__(self):
          return u"message from %s" % self.by_user


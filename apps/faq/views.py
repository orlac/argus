# -*- coding:utf8 -*-
from datetime import datetime

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import Http404
from django.shortcuts import redirect

from faq.models import *
from faq.forms import *

def list(request, ordering="-by_date", template_name="faq_list.html", redir_url='/faq', extracontext={}):
    try:
        query=Answer.objects.filter(public=True).order_by(ordering)
    except:
        query=False
    if request.method == 'POST':
        #print request.POST
        form = FaqForm(request.POST)
        if form.is_valid():
            q = Quest(name = form.cleaned_data['name'],
			email = form.cleaned_data['email'],
			text = form.cleaned_data['quest'],                                          
			by_date = datetime.now(),
			user = request.user,
                     )
            q.save()                           
            return redirect(redir_url)
    else:        
		form = FaqForm()
    return render_to_response(template_name, {'query':query, 'form':form}, context_instance=RequestContext(request, extracontext))


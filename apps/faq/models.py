﻿# -*- coding:utf-8 -*-
from datetime import datetime

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _



class Quest(models.Model):
    user = models.ForeignKey(User, null=True, blank=True)
    name = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    text = models.TextField(max_length=500, )
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)
    public = models.BooleanField(_('Public'))



    def __unicode__(self):
        return u"%s | %s" % (self.name, self.text)


    class Meta: 
        db_table = settings.DBPREFIX+'Quest'
        verbose_name = "Вопросы"
        verbose_name_plural = "Вопрсы"
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """


class Answer(models.Model):
    quest = models.ForeignKey(Quest,)
    user = models.ForeignKey(User,)
    text = models.TextField(max_length=500, )
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)
    public = models.BooleanField(_('Public'))



    def __unicode__(self):
        return u"%s |"  % (self.user)


    class Meta: 
        db_table = settings.DBPREFIX+'Answer'
        verbose_name = "Ответы"
        verbose_name_plural = "Ответы"





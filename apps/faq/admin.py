﻿# -*- coding:utf-8 -*-
from django.contrib import admin
from faq.models import *


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 1


class AnswerAdmin(admin.ModelAdmin):
    raw_id_fields = ['quest', 'user',]
    list_display = ['quest', 'user', 'public',]
    pass


class QuestAdmin(admin.ModelAdmin):

    inlines = [
        AnswerInline,
       ]

    raw_id_fields = ['user']
    list_filter = ('user', 'by_date',)

    #Кнопка сохранить вверху
    save_on_top = True
    pass


admin.site.register(Quest, QuestAdmin)
admin.site.register(Answer, AnswerAdmin)

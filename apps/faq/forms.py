# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

class FaqForm(forms.Form):
	name = forms.CharField(label="Представтесь",)
	email = forms.EmailField(label="Контактный e-mail",)
	quest = forms.CharField(label="Вопрос", widget=forms.Textarea(attrs={'class':'TextQuest'}),)
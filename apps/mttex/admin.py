# -*- coding:utf-8 -*-
from mttex.models import *
from django.contrib import admin
#from mpttadmin import MpttAdmin

"""
class MyModelAdmin(MpttAdmin):
    tree_title_field = 'name'
    tree_display = ('name','content',) #name тут указывать необязательно

    class Meta:
        model = Catmtt
"""


class CatmttAdmin(admin.ModelAdmin):    
    list_display = ('name', 'content')
    #list_filter = ('parent')
    #fields = ('user', 'name', 'by_date', 'text',)

    save_on_top = True
    pass
    

#admin.site.register(Catmtt, CatmttAdmin)
# -*- coding:utf8 -*-
from datetime import datetime

from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse
from django.http import HttpRequest

from help.models import *
from news.models import *
from staticpages.models import *
from downloads.models import * 


class MySitemap(Sitemap):
    
    def __get(self, name, obj, default=None):
        try:
            attr = getattr(self, name)
        except AttributeError:
            return default
        if callable(attr):
            return attr(obj)
        return attr

    def get_urls(self, page=1):       
        urls = []
        for item in self.paginator.page(page).object_list:
            #loc = "http://%s%s" % (current_site.domain, self.__get('location', item))
            host = HttpRequest.get_host
            loc = "http://%s%s" % (host, self.__get('location', item))
            url_info = {
                'location':   loc,
                'lastmod':    self.__get('lastmod', item, None),
                'changefreq': self.__get('changefreq', item, None),
                'priority':   self.__get('priority', item, None)
            }
            urls.append(url_info)
        return urls


class HelpSitemap(MySitemap):
    """ * """
    
    changefreq = 'weekly'
    priority = 0.5

    def items(self):
        return Category.objects.all()

    def lastmod(self, obj):
        return datetime.now


    def location (self, obj):
        return reverse('help_list', args=[obj.id])
        
        

class NewsSitemap(MySitemap):
    changefreq = 'weekly'
    priority = 0.5

    def items(self):
        return News.objects.all()

    def lastmod(self, obj):
        return datetime.now


class StaticpagesSitemap(Sitemap):
    changefreq = 'weekly'
    priority = 0.5

    def items(self):
        return StaticPages.objects.all()

    def lastmod(self, obj):
        return datetime.now 

class DownloadsSitemap(MySitemap):
    changefreq = 'weekly'
    priority = 0.5

    def items(self):
        return Versions.objects.all()

    def lastmod(self, obj):
        return datetime.now         
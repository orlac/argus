# -*- coding:utf8 -*-
import time
t = time.localtime()

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.core import serializers
from django.contrib.auth.decorators import *

from faqch.models import *


#@login_required()#return HttpResponse('login':False)
def set_online(request, extracontext={}):
	time.sleep(3)
	try:
		o = Online.objects.get(user = request.user)
		o.user = request.user
		o.last_date = time.mktime(t)
		#print o
	except:
		o = Online(user = request.user)
	o.save()		
	#data = Online.objects.select_related('user__username', 'user').filter(user__is_staff=True, last_date__gt = int(time.mktime(t))-60 ) #60 seconds
	#print data
	data = serializers.serialize('json', Online.objects.select_related('user__id', 'user__name').filter(user__is_staff=True, last_date__gt = int(time.mktime(t))-60 ))
	#ret = serializers.serialize('json', {'login':True, 'data':data})
	ret = [{'login':True, 'data':data}]
	return HttpResponse(ret, mimetype='application/json')
	#return HttpResponse(ret)

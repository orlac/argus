# -*- coding:utf-8 -*-
from datetime import datetime
import time

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

t = time.localtime()


class Messgaes(models.Model):
    user = models.ForeignKey(User, related_name='from user')
    touser = models.ForeignKey(User, related_name='to user')        
    text = models.TextField(max_length=500, )
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)    



    def __unicode__(self):
        return u"%s | %s" %(self.user, self.user)


    class Meta: 
        db_table = settings.DBPREFIX+'chatMessages'
        verbose_name = "Messgaes"
        verbose_name_plural = "Messgaes"
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """


class Online(models.Model):
    user = models.ForeignKey(User,)        
    last_date = models.FloatField(default=time.mktime(t),)    



    def __unicode__(self):
        return u"%s | %s" %(self.user, self.last_date)


    class Meta: 
        db_table = settings.DBPREFIX+'chatOnline'
        verbose_name = "Online"
        verbose_name_plural = "Online"





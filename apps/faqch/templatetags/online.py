# -*- coding:utf8 -*-
import time
t = time.localtime()

from django.template import Library, Node
register = Library()

from faqch.models import *


def load_users_online(parser, token):
    """
    {% load_users_online [optin] as [context_var] %}
    	option: staff, all
    """
    bits = token.contents.split()
    if len(bits) not in (1, 2, 4):
        raise TemplateSyntaxError('%s tag requires none, one or three arguments' % bits[0])
    if bits[2] != 'as':
        raise TemplateSyntaxError("Second argument to %s tag must be 'as'" % bits[0])
    if not bits[1]:
        bits[1] = 5 # Default number of items
    if not bits[3]:
        bits[3] = 'latest_posts'
    return load(bits[1], bits[3])

class load(Node):
    def __init__(self, option, context_var):
        self.option = option
        self.context_var = context_var
    
    def render(self, context):
		if self.option is 'staff':			                
			context[self.context_var] = Online.objects.filter(user__is_staff=True, last_date__gt = int(time.mktime())-60 ) #60 seconds
			#last_date>(int(time.mktime())-60)
		return ''



register.tag(load_users_online)
# -*- coding:utf8 -*-
from datetime import datetime

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import Http404

from downloads.models import *

def list(request, ordering="-by_date", template_name="loads_list.html", extracontext={}):
    try:
        query=Versions.objects.all().order_by(ordering)
    except:
        query=False    
    return render_to_response(template_name, {'query':query}, context_instance=RequestContext(request, extracontext))
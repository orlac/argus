# -*- coding:utf-8 -*-
from datetime import datetime

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class Versions(models.Model):
    name = models.CharField(max_length=100, )
    text = models.TextField(max_length=500, null=True, blank=True)
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)
    file = models.FileField(upload_to = 'files/argus')



    def __unicode__(self):
        return u"%s | %s" %(self.name, self.by_date)


    def get_absolute_url(self):
        #return reverse('load_list')
        return "%s%s" % (settings.MEDIA_URL, self.file)


    class Meta: 
        ordering = ['-by_date']
        db_table = settings.DBPREFIX+'versions'
        verbose_name = _("Versions")
        verbose_name_plural = _("Argus")
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """


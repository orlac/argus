# -*- coding:utf-8 -*-
from django.contrib import admin
from downloads.models import *

class VersionsAdmin(admin.ModelAdmin):
    pass


admin.site.register(Versions, VersionsAdmin)

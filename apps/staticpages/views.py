# -*- coding:utf8 -*-
from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
#from django.shortcuts import redirect
from django.http import HttpResponseRedirect as redirect

from staticpages.forms import ContactForm
from staticpages.models import *

def viewpage(request, _typepage='AG', template_name = 'staticpage.html', redir_url='/', extracontext = {}, contacts = False):
    #print _typepage
	try:
		query = StaticPages.objects.get(typepage=_typepage)        
	except:
		query = {
			'name' : 'Argus',
			'text' : 'Страница на стадии заполнения контентом',
			'contacts' : True
        	}
		contacts = True
		#print query    
        #print query.contacts
        
    #print extracontext
	
	form=False
	
	if request.method == 'POST':
		form = ContactForm(request.POST)
		if form.is_valid():				                 
			return redirect(redir_url)
	else:        
		if contacts :
			form = ContactForm()
		else:
			form = False	    
	
	
	return render_to_response(template_name, {'query': query, 'form':form}, context_instance=RequestContext(request, extracontext))
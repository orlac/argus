# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.models import User

from tinymce.widgets import TinyMCE

from staticpages.models import *

class ContactForm(forms.Form):
	name = forms.CharField(label="Представтесь",)
	email = forms.EmailField(label="Контактный e-mail",)
	quest = forms.CharField(label="Сообщение", widget=forms.Textarea(attrs={'class':'TextQuest'}),)
	"""
	def clean(self):		
		try:
			u = User.objects.get(is_superuser==True)
			send_mail('Contacts', self.cleaned_data['quest'], self.cleaned_data['email'], [u.email], fail_silently=False)			
		except:
			raise forms.ValidationError(_("Не удалось отправить сообщение, попробуйте в другой раз"))			
		return self.cleaned_data
	"""


class StaticPageForm(forms.ModelForm):            
	#text = forms.CharField(widget=TinyMCE(attrs={'cols': 80, 'rows': 30}))
	
	class Media:
		css = { 'all': ('/media/js/jwysiwyg/jquery.wysiwyg.css')
				}
		js = ('/media/js/jwysiwyg/jquery.wysiwyg.js',
				'/media/js/jquery-1.4.2.min.js',
				'/media/js/textarea-edit.js',
				)		
	
	class Meta:
		model = StaticPages
	pass
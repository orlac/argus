# -*- coding:utf-8 -*-
from django.contrib import admin, sites
from staticpages.models import *
from staticpages.forms import *


class SorlInline(admin.TabularInline):    
    list_display = ('photo',)
    model = SorlPhotos
    extra = 3

class StaticAdmin(admin.ModelAdmin):
    inlines = [
        SorlInline,
       ]
    form = StaticPageForm
    pass


admin.site.register(StaticPages, StaticAdmin)
#admin.site.unregister(sites)

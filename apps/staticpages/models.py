﻿# -*- coding:utf-8 -*-
from datetime import datetime

from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.core.urlresolvers import reverse

#from tinymce import models as tinymce_models
from othersettings import *
from sorl.thumbnail.fields import *


class StaticPages(models.Model):
    typepage = models.CharField(max_length=100, choices=STATIC_PAGES, unique=True)
    name = models.CharField(max_length=100, )
    text = models.TextField(max_length=500, )
    #text = tinymce_models.HTMLField(max_length=500, )
    contacts = models.BooleanField(_('contact form'), default=False, help_text=_("contact form to page"))



    def __unicode__(self):
        return u"%s | %s" %(self.name, self.typepage)


    def get_phots(self):
        return SorlPhotos.objects.filter(page = self)
    
    def get_absolute_url(self):
        types = {
            'AG': 'about',
            'AC': 'company',
            'CN': 'contacts',
        }            
        return reverse(types[self.typepage])



    class Meta: 
        db_table = settings.DBPREFIX+'StaticPages'
        verbose_name = "Статические страници"
        verbose_name_plural = "Страницы"
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """

class SorlPhotos(models.Model):
    page = models.ForeignKey('StaticPages',)    
    photo = ThumbnailField(upload_to='Sorlfiles/pages',
                           size=(950, 950),
                           extra_thumbnails={
                           'icon': {'size': (50, 50)},
                           }
                       )
    name = models.CharField(max_length=50, null=True, blank=True)


    class Meta: 
        db_table = settings.DBPREFIX+'pages_SorlPhoto'
        verbose_name = _("PagesPhoto")
        verbose_name_plural = _("PagesPhoto")
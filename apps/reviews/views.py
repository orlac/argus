# -*- coding:utf8 -*-
import datetime

from django.template import Template, Context, RequestContext
from django.shortcuts import render_to_response
from django.http import Http404
#from django.shortcuts import redirect
from django.http import HttpResponseRedirect as redirect

from reviews.models import *
from reviews.forms import *
#from django.contrib.auth.decorators import login_required


"""
def search(request, template_name='doplace/search.html', extracontext={}):
    query=False
    if request.method =='POST':
        form = SearchForm(request.POST)
        if form.is_valid():
            query=query_list(request, data=form.cleaned_data)
    else:
        form = SearchForm()
    return render_to_response(template_name, {'query':query, 'form_search':form}, context_instance=RequestContext(request, extracontext))
"""

def list(request, ordering="-by_date", template_name="reviews_list.html", redir_url='/reviews', extracontext={}):

    try:
        query=Reviews.objects.all().order_by(ordering)
    except:
        query=False
    if request.method == 'POST':
    	if request.user.is_authenticated():		
        	form = ReviewForm(request.POST)
        	if form.is_valid():
				q = Reviews(text = form.cleaned_data['quest'],                                                               
    					  	user = request.user,
                    	 	)
            	q.save()                           
            	return redirect(redir_url)
    	else:
    		return redirect("/accounts/login/")
    else:        
		form = ReviewForm()
    return render_to_response(template_name, {'query':query, 'form':form}, context_instance=RequestContext(request, extracontext))



def show(request, idreview=0, template_name="reviews_show.html", extracontext={}):
    idreview = int(idreview)
    if idnews != 0:
        try:
            query=Reviews.objects.get(id=idreview)
        except:
            query=False
    else:
        query=False
    return render_to_response(template_name, {'query':query}, context_instance=RequestContext(request, extracontext))






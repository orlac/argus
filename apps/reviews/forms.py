# -*- coding:utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _

class ReviewForm(forms.Form):	
	quest = forms.CharField(label=_("Quest"), widget=forms.Textarea(attrs={'class':'TextQuest'}),)
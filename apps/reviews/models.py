# -*- coding:utf-8 -*-
from datetime import datetime
import random

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class Reviews(models.Model):
    user = models.ForeignKey(User, )        
    text = models.TextField(max_length=500, )
    by_date = models.DateTimeField(auto_now_add=False, default=datetime.now)    


    def __unicode__(self):
        return u"%s" % self.user            

    class Meta: 
        ordering = ['-by_date']
        db_table = settings.DBPREFIX+'reviews'
        verbose_name = "Отзывы"
        verbose_name_plural = "Отзывы"
        #Права доступа к записям
        """
        permissions = (
            ("has_new", _("Add news of User")), #Добавление
            ("has_del", _("Delete news of User")), #Удаление
            ("has_change", _("Change news of User")), #Редактирование
        )
        """

# -*- coding:utf-8 -*-
from django.contrib import admin
from django.contrib.comments.models import *


from reviews.models import *


class ReviewsAdmin(admin.ModelAdmin):
    
    list_display = ('user', 'by_date')
	#raw_id_fields = ['user',]
    list_filter = ('user', 'by_date',)
    fields = ('user', 'by_date', 'text',)

    """formfield_overrides = {
        models.TextField: {'widget': RichTextEditorWidget},
    }"""
	#fields = ('user', 'submit_date', 'comment', 'is_public', 'is_removed')
    #radio_fields = {"parent": admin.HORIZONTAL}
    #Кнопка сохранить вверху
    save_on_top = True
    pass

admin.site.register(Reviews, ReviewsAdmin)
 

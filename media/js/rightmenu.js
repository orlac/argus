$(function(){
  $("#accordion").accordion({  	
  	clearStyle: true,
  	collapsible: true,
  	autoHeight: false,
  	navigation: true
  });
  $("#accordion li a").click(function(){
  	$.cookie("openItem", $(this).attr("href"));
  });
  $("#accordion li a[href$='" + $.cookie("openItem") + "']").addClass("open");
  
});
$(function(){
  $("#review_accordion").accordion({  	
  	clearStyle: true,
  	collapsible: true,
  	autoHeight: false,
  	navigation: true
  });
  
  $("#review_accordion a").click(function(){
  	$.cookie("openItem", $(this).attr("href"));
  });
  $("#review_accordion a[href$='" + $.cookie("openItem") + "']").addClass("open");
 
});

/*
$('.review-title').click(function(){	
	$('#form_'+$(this).attr('id')).show("slow");
	$(this).click(function(){
		$('#form_'+$(this).attr('id')).hide("slow");
	})	
;
});

$('.text_review').click(function(){
	alert();	
	$(this).hide("slow");
})
*/
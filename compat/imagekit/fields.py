# -*- coding:utf8 -*-
import sys, os

from PIL import Image
from django.conf import settings

from django.db import models

class GaleryField(models.ImageField):
     #attr_class = models.ImageField
     def __init__(self, verbose_name=None, name=None, thumb_size=None , **kwargs):
        self.thumb_size = thumb_size
        models.ImageField.__init__(self, verbose_name, name, **kwargs)

     """def __init__(self, thumb_size=None):
          self.thumb_size = thumb_size
          """
     def imageResize(self, data, output_size):
          """
          Resize image for thumbnails and preview
          data — image for resize
          output_size — turple, contains width and height of output image, for example (200, 500)
          """

          image = Image.open(data)
          m_width = float(self.thumb_size['x'])
          m_height = float(self.thumb_size['y'])
          if image.mode not in ('L', 'RGB'):
             image = image.convert('RGB')
          w_k = image.size[0]/m_width
          h_k = image.size[1]/m_height
          if output_size < image.size:
            if w_k > h_k:
               new_size = (m_width, image.size[1]/w_k)
            else:
               new_size = (image.size[0]/h_k, m_height)
          else:
             new_size = image.size
          return image.resize(new_size,Image.ANTIALIAS)


     def save_form_data(self, instance, data):
          from StringIO import StringIO
          from django.core.files.uploadedfile import SimpleUploadedFile, UploadedFile

          if data and isinstance(data, UploadedFile):
              image = self.imageResize(data, self.thumb_size)
              new_image = StringIO()
              image.save(new_image, 'JPEG', quality=85)
              data = SimpleUploadedFile(data.name, new_image.getvalue(), data.content_type)

              # Удаление старого файла
              previous = u'%s%s' % (settings.MEDIA_ROOT, self.name)
              if os.path.isfile(previous):
                os.remove(previous)
              # -
          super(GaleryField, self).save_form_data(instance, data)



     def make_upload_path(instance, filename, prefix = False):
          # Переопределение имени загружаемого файла.
          from utils.hashfunc import get_hash

          filename = 'a_' + get_hash('md5') + '.jpg'
          return u"%s/%s" % (models.FileField.upload_to, filename)


     """
     def delete_file(self, instance, sender, **kwargs):
        file = getattr(instance, self.attname)
        if file and file.name != self.default and \
            not sender._default_manager.filter(**{self.name: file.name}):
                file.delete(save=False)
        elif file:
            file.close()"""

#

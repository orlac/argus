# -*- coding:utf-8 -*-
# Django settings for argusp project.
########################################
import os.path
import sys
import ConfigParser
config = ConfigParser.ConfigParser()
config.read('config.ini')
#import os.sep
####Other settings##########################
#####################################
NEWSPAGE = int(config.get('news', 'TOPAGE'))
FAQSPAGE = int(config.get('faq', 'TOPAGE'))
REVIEWPAGE = int(config.get('review', 'TOPAGE'))
#�����������_________________________________________________________
#����� ����� ���������� ��������
ACCOUNT_ACTIVATION_DAYS = config.get('account', 'ACCOUNT_ACTIVATION_DAYS')
#��������� ��� �� �����
AUTH_USER_EMAIL_UNIQUE = config.get('account', 'AUTH_USER_EMAIL_UNIQUE')
#��������� �������
EMAIL_HOST = config.get('account', 'EMAIL_HOST')
EMAIL_PORT = int(config.get('account', 'EMAIL_PORT'))
EMAIL_HOST_USER = config.get('account', 'EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config.get('account', 'EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = config.get('account', 'EMAIL_USE_TLS')
DEFAULT_FROM_EMAIL = config.get('account', 'DEFAULT_FROM_EMAIL')

LOGIN_REDIRECT_URL = config.get('account', 'LOGIN_REDIRECT_URL') #�������� ��� �����������
AUTH_PROFILE_MODULE = config.get('account', 'AUTH_PROFILE_MODULE')
######################################
######################################

PROJECT_ROOT = os.path.normpath(os.path.dirname(__file__))
THEME = "argus"

sys.path.insert(0, os.path.join(PROJECT_ROOT, 'apps'))
sys.path.insert(0, os.path.join(PROJECT_ROOT, 'compat'))
#########################################
#from othersettings import *
#NEWSPAGE = 1


DEBUG = True
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)

MANAGERS = ADMINS

DATABASE_ENGINE = 'sqlite3'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = os.path.join(PROJECT_ROOT, 'db/argus.db')             # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

DBPREFIX = 'argus_'

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Moscow'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'ru'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = '/media/'
STATIC_URL = '/static/'

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = '/admin-media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'aec49tbl0pqr7_@n84q2#x2v8#do6gdt(z=it99kh%943wh3vk'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',    
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',    
    'django.middleware.locale.LocaleMiddleware',
    'pagination.middleware.PaginationMiddleware',
    #'django.contrib.auth.middleware.AuthenticationMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "context_processors.settings_vars",
]


ROOT_URLCONF = 'urls'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, 'templates', THEME),
    os.path.join(PROJECT_ROOT, 'templates', THEME, 'forum'),
    os.path.join(PROJECT_ROOT, 'compat/treebeard/templates'),
    os.path.join(PROJECT_ROOT, 'templates'),
	# Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.markup',
    'django.contrib.sitemaps',
    
    #'mptt',
	'treebeard',
    'pagination',
    'registration',
    #'simplewiki',
    'forum',
    #'tbexample',
    #'photologue',
    #'imagekit',
    'sorl.thumbnail',
    
    'news',
    'downloads',
    'staticpages',
    'faq',
    'arguswiki',
    'help',
    'reviews',
	'faqch',
    'tinymce',
    'pyamftest',
)
